package com.example.game.gamehit;

/**
 * Created by Administrator on 2017/12/7.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class GameActivity extends Activity {

    //音乐播放器
	private MediaPlayer mBgMediaPlayer;
	private boolean isMusic = true;
	private Context mContext;
	public boolean isPause;
	public GameView gameView;
	private Realm realm;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//无标题
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);//全屏
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);//应用运行时，保持屏幕常亮，不锁屏
		//初始化
		init();
        //进入游戏界面
		gameView = new GameView(GameActivity.this);
        setContentView(gameView);
    }

	private void init() {
		mContext = GameActivity.this;
		//播放背景音乐
		mBgMediaPlayer = MediaPlayer.create(mContext, Const.voiceBackground);
		mBgMediaPlayer.setLooping(true);
	}

	//设置按返回键的操作
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	switch (event.getKeyCode()) {
		case KeyEvent.KEYCODE_BACK:
			//按返回键时，调用returnMenu方法
			returnMenu();
			return false;
		}
		return super.onKeyDown(keyCode, event);
    }

    //弹出Dialog框，提示是否返回菜单，并保存当前成绩
    private void returnMenu(){
		isPause = true;
		TextView textView = new TextView(mContext);
		textView.setText("要返回菜单吗？当前成绩(除0分)将会保存");
		textView.setTextSize(24);
		textView.setGravity(Gravity.CENTER);
		textView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
		textView.setTextColor(getResources().getColor(R.color.fontColor));
		new AlertDialog.Builder(mContext)
				.setCustomTitle(textView)
				.setPositiveButton("否", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						isPause = false;
					}
				})
				.setNegativeButton("是", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//如果分数大于0，保存成绩
						if (gameView.getScore()>0) {
							stroeNumber(gameView.getScore());
						}
						finish();
					}
				})
				.show();
	}

    //游戏结束时，显示成绩、时间等相关数据
	public void showTimerGrade(final int grade, int killnum){
    	Builder timerGrade = new Builder(mContext);
    	TextView textView = new TextView(mContext);

    	textView.setTextColor(getResources().getColor(R.color.fontColor));
    	textView.setTextSize(24);
    	textView.setGravity(Gravity.CENTER);
    	textView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));

        //如果分数大于目标分数
    	if(grade > gameView.target){
			isPause = true;
			textView.setText("恭喜过关！\n"+"本次"+Const.timeNum+"秒计时\r\n击中："+killnum+"个\r\n得分："+grade+"分");
			timerGrade.setView(textView);
			timerGrade.setPositiveButton("下一关", new DialogInterface.OnClickListener() {
				@Override//如果点击下一关
				public void onClick(DialogInterface dialog, int which) {
					gameView.showDishuTime=100;
					gameView.target += 500;
					gameView.initGameInfo();
					stroeNumber(grade);
				}
			});
		}else{//如果分数不够
			textView.setText("GAME OVER\n"+"本次"+Const.timeNum+"秒计时\r\n击中："+killnum+"个\r\n得分："+grade+"分");
			timerGrade.setView(textView);
			timerGrade.setPositiveButton("重新挑战", new DialogInterface.OnClickListener() {
				@Override//储存分数，重新游戏
				public void onClick(DialogInterface dialog, int which) {
					gameView.initGameInfo();
					stroeNumber(grade);
				}
			});
		}

		//结束活动，返回菜单
    	timerGrade.setNegativeButton("返回菜单", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(grade != 0){
					stroeNumber(grade);
				}
				finish();
			}
		});
		//设置不能按返回键取消
    	timerGrade.setCancelable(false);
    	timerGrade.show();
    }

    //储存数据
    private void stroeNumber(final int number){
		Realm.init(this);
		RealmConfiguration config = new RealmConfiguration.Builder()
				.name("realmnum.realm")
				.schemaVersion(0)
				.build();
		realm = Realm.getInstance(config);
		realm.executeTransaction(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				//把数据存到Ranking中
				Ranking user = realm.createObject(Ranking.class);
				user.setNumber(number);
			}
		});

	}
    
    //播放背景音乐
    public void playBackgroundMusic(){
    	if(Const.backgroundMusicOn && mBgMediaPlayer!=null){
			mBgMediaPlayer.start();
    	}else{
    		if(mBgMediaPlayer.isPlaying()){
    			mBgMediaPlayer.pause();
    		}
    	}
    }

    //销毁活动，关闭音乐
    @Override
    protected void onDestroy() {
    	if (this.mBgMediaPlayer != null) {
    		if (this.isMusic){
    			this.mBgMediaPlayer.stop();
    			this.mBgMediaPlayer.release();
    			this.mBgMediaPlayer = null;
    		}
        }
        try {
    		realm.close();
		}catch (Exception e){
    		e.printStackTrace();
		}
		super.onDestroy();
    }
}
