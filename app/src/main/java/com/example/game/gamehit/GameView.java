package com.example.game.gamehit;

/**
 * Created by Administrator on 2017/12/7.
 */

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameView extends View implements Runnable{

	public GameActivity mContext;
	public boolean running;
	public UpDownAnima uda;
	public List<UpDownAnima> udaList = new ArrayList<UpDownAnima>();
	public long sleepTime = 50;
	public long showDishuTime = 500;
	private int touchX;
	private int touchY;
	public String num="目标分数：";
	public int target = 500;
	private Bitmap imgBackground;
	private Bitmap imgDishu;
	private Bitmap imgDidong;
	private int[][] didongArray;
	private int rowSize = 17;
	private int colSize = 10;
	private int count;
	private int killNum=0;
	private Bitmap imgNumber;
	private Bitmap imgSmallDishu40;
	private Bitmap imgX;
	private boolean hitting;//判断是否点击屏幕
	private Bitmap imgMe;
	private int score;
	private Bitmap imgScore;
	private Bitmap imgTimer;
	private int timeCount;
	private int timeNum;

	// 启动游戏界面
	public GameView(GameActivity context) {
		super(context);
		mContext = context;
		running = true;
		mContext.isPause = false;
		initBitmap();//载入资源，主要是图片资源；
		initGameInfo();// 初始化游戏时间、分数、关卡等数据。
		//用SharedPreferences储存是否播放音乐的数据
		SharedPreferences sharedPreferences=getContext().getSharedPreferences("music", Activity.MODE_PRIVATE);//储存数据
		boolean isplay=sharedPreferences.getBoolean("play",true);
		//如果获取的值为true，播放音乐
		if (isplay){
			playBackMusic();
		}
        //线程开始
		new Thread(this).start();
	}
	
    //播放背景音乐
	private void playBackMusic() {
		//调用GameActivity中的方法
		mContext.playBackgroundMusic();
	}

	//初始化游戏信息
	public void initGameInfo() {
        //清除列表
		udaList.clear();
		//初始化时间，分数等变量
		score = 0;
		timeNum = Const.timeNum;
		killNum = 0;
		mContext.isPause = false;
	}

	//重写onDraw方法，画出游戏界面
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		//画出背景图
		canvas.drawBitmap(imgBackground, 0, 0, null);
		//画出顶部的菜单
		topMenu(canvas);
		//画出地鼠
		playAnima(canvas);
		//画出目标分数
		drawtext(canvas);
	}

    private void drawtext(Canvas canvas){
		Paint paint=new Paint();
		//抗锯齿
		paint.setAntiAlias(true);
		//设置字体大小
		paint.setTextSize(70);
		//设置字体颜色
		paint.setColor(getResources().getColor(android.R.color.holo_red_light));
		//设置笔刷的粗细度
		paint.setStrokeWidth(5);
		//设置画笔的样式:填充和描边
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		DisplayMetrics dm = getResources().getDisplayMetrics();//获取手机参数
		canvas.drawText(num+target,dm.widthPixels*4/9,dm.heightPixels*3/4,paint);
	}


	private void topMenu(Canvas canvas) {
		//用来记录显示这些图片所占的宽度
		int marginLeft = 1;
		//显示打了多少地鼠
		marginLeft = dishuInfo(canvas, marginLeft);
		//显示当前得分
		marginLeft = scoreInfo(canvas, marginLeft);
		//显示当前剩余时间
		timeInfo(canvas, marginLeft);
	}

	private int scoreInfo(Canvas canvas, int marginLeft) {
		marginLeft += 1;
		//在背景画布上画出得分的图片
		canvas.drawBitmap(imgScore, marginLeft, 5, null);
		//记录宽度
		marginLeft += imgScore.getWidth() + 3;
		//画出具体得分的数字
		marginLeft = drawNum(canvas, score, marginLeft, 7);
		return marginLeft;
	}

	private int dishuInfo(Canvas canvas, int marginLeft) {
		marginLeft += 1;
		//画出“地鼠”图片
		canvas.drawBitmap(imgSmallDishu40, marginLeft, 5, null);
		marginLeft += imgSmallDishu40.getWidth() + 1;
		//画出“X”图片
		canvas.drawBitmap(imgX, marginLeft, 15, null);
		marginLeft += imgX.getWidth() + 1;
		//画出打到的地鼠个数
		marginLeft = drawNum(canvas, killNum, marginLeft, 7);
		return marginLeft;
	}


	private int timeInfo(Canvas canvas, int marginLeft) {
		//如果时间<=0，活动暂停，显示本关结束后的相关信息
		if(timeNum<=0){
			mContext.isPause=true;
			mContext.showTimerGrade(score, killNum);
		}
		marginLeft += 1;
		//画出“时间”的图片
		canvas.drawBitmap(imgTimer, marginLeft, 5, null);
		marginLeft += imgTimer.getWidth();
		//画出当前剩余时间的数字
		drawNum(canvas, timeNum, marginLeft, 7);
		return marginLeft;
	}

	//用来画出变化的阿拉伯数字
	private int drawNum(Canvas canvas, int num, int marginLeft, int marginTop){
		//把传入的数字转化成字符串，如果小于0则传入0
		String numStr = String.valueOf(num<0?0:num);
		Bitmap imgNum;
		//把数字的每一位数都转化成图片
		for(int i=0; i<numStr.length(); i++){
			imgNum = getNumberImg(Integer.valueOf(numStr.substring(i, i+1)));
			canvas.drawBitmap(imgNum, marginLeft, marginTop, null);
			marginLeft += imgNum.getWidth() + 1;
		}
		return marginLeft;
	}

	//把传入的数字转化成图片
	private Bitmap getNumberImg(int num){
		//数字图片宽度/10
		int numW = imgNumber.getWidth()/10;
		//转化成图片
		return Bitmap.createBitmap(imgNumber, num*numW, 0, numW, imgNumber.getHeight());
	}

	public void playAnima(Canvas canvas){
		//把列表中的每一个地鼠的上下跳动都画出来
		for(UpDownAnima uda : udaList){
			uda.drawFrame(canvas, null);
		}
		List<UpDownAnima> rm = new ArrayList<UpDownAnima>();
		for(UpDownAnima uda : udaList){
			//如果被打到
			if(uda.isHit()){
				rm.add(uda);
			}else if(!uda.isHitable()){//如果不能打了，即错过了打击时间
				rm.add(uda);
			}
		}
		//删除这些地鼠
		udaList.removeAll(rm);
	}

    //重写run方法
	@Override
	public void run() {
		while (running) {
			try {
				//线程休眠
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//如果不暂停
			if(!mContext.isPause){
				//在线程中更新界面
				postInvalidate();
				count++;
				//每隔0.5秒出现一个地鼠
				if(count>=showDishuTime/sleepTime){
					//获取地鼠
					genDishu();
					count=0;
				}
				timeCount++;
				//每隔一秒，倒计时时间减一
				if(timeCount*sleepTime==1000){
					timeCount = 0;
					timeNum--;
				}
			}
		}
	}

    //重写触屏事件
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN://按下时
			//获取按下位置的坐标
			touchX = (int) event.getX();
			touchY = (int) event.getY();
			//表示正在打击
			hitting = true;
			//判断是否击中
			isHit();
			break;
		}
		return true;
	}


	private void isHit() {
		for(UpDownAnima uda : udaList){
			//如果正在打击且击中
			if(hitting && uda.isHit(touchX, touchY)){
				hitting = false;
				//加分或减分
				score+=uda.getScore();
				//如果击中得到地鼠是加分的
				if(uda.isAddFlag()){
					//击中地鼠的数量加一
					killNum++;
				}
			}
		}
		//重新获得地鼠
		genDishu();
	}

	//判断是否随机显示
	private boolean randomShow(int row, int col){
		double random = Math.random();
		boolean flag = false;
		if(0.9>random){
			flag = true;
		}
		for(UpDownAnima uda : udaList){
			if(uda.getRow()==row && uda.getCol()==col){
				flag = false;
			}
		}
		return flag;
	}
	
	private void genDishu(){
		didongArray = loadMapArray(Const.gameArrayStrResid, rowSize, colSize);
		int rowSize = didongArray.length;
		int colSize = didongArray[0].length;
		int randomDishu;
		Random random = new Random();
		randomDishu = random.nextInt(6)+1;
		Random rand = new Random();
		double randShow;

		for(int row=0; row<rowSize; row++){
			for(int col=0; col<colSize; col++){
				if(randomDishu==didongArray[row][col]){
					if(randomShow(row, col)){
						randShow = rand.nextDouble();
						if(randShow>0.2){
							uda = new UpDownAnima(imgDishu,imgDidong, 100, 8, (int) (col*Const.CURRENT_BLOCK_WIDTH), (int) (row*Const.CURRENT_BLOCK_HEIGHT), true);
						}else{
							uda = new UpDownAnima(imgMe,imgDidong, 100, 8, (int) (col*Const.CURRENT_BLOCK_WIDTH), (int) (row*Const.CURRENT_BLOCK_HEIGHT), false);
						}
						uda.setRow(row);
						uda.setCol(col);
						udaList.add(uda);
					}
				}
			}
		}
	}

	//载入各种地图资源,如背景、老鼠、地洞、音乐的资源
	private void initBitmap(){
		DisplayMetrics dm = getResources().getDisplayMetrics();//获取手机参数
		//设置游戏界面的宽度与高度为手机的屏幕的大小
		int mScreenWidth = dm.widthPixels;
		int mScreenHeight = dm.heightPixels;
		//获取在游戏界面要用到的各种图片资源
		Bitmap bmp = getImgByResId(Const.backgroundImgResid);//获取背景图
		imgBackground = Bitmap.createScaledBitmap(bmp, mScreenWidth, mScreenHeight, true);//设置背景图
		imgDishu = getImgByResId(R.drawable.dishu_40x60);//获取地鼠的图
		imgDidong = getImgByResId(R.drawable.dd_dc);// 获取地洞的图片
		imgNumber = getImgByResId(R.drawable.num);//获取有关数字的图
		imgSmallDishu40 = getImgByResId(R.drawable.smalldishu_40);//获取小地鼠的图片
		imgX = getImgByResId(R.drawable.x);//获取“X”的图片
		imgMe = getImgByResId(R.drawable.me_48);//获取扣分地鼠的图片
		imgScore = getImgByResId(R.drawable.score);//获取“得分”的图片
		imgTimer = getImgByResId(R.drawable.timer);//获取“时间”的图片
	}

	//根据资源ID取图片
	private Bitmap getImgByResId(int resid){
		//用BitmapFactory获取图片资源
		Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), resid);
		//如果要缩放的比例与默认缩放比例不同则进行图片缩放
		if(Const.DEF_SCALE!=Const.CURREN_SCALE){
			int sImgWidth = bitmap.getWidth();
			int sImgHeight = bitmap.getHeight();
			//产生reSize后的Bitmap对象
			Matrix matrix = new Matrix();
			matrix.postScale(Const.CURREN_WIDTH_SCALE, Const.CURREN_HEIGHT_SCALE);
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, sImgWidth, sImgHeight, matrix, true);
		}
		return bitmap;
	}

	//加载地图数组
	private int[][] loadMapArray(int strResid, int rowSize, int colSize)
	{
		int[][] mapArray;
		String str;
		str = mContext.getResources().getString(strResid);//获取String形式的资源id
		mapArray = new int[rowSize][colSize];// 确定地图的行数与列数
		String[] arr = str.split(",");//将字符串用“，”为结点划分
		int temp ;
		int max = 0;
		for(int row=0; row<rowSize; row++){
			for(int col=0; col<colSize; col++){
				temp = row*colSize + col;
				mapArray[row][col] = Short.valueOf(arr[temp]);
			}
		}
		return mapArray;//返回地图数组
	}

	//获取分数
	public int getScore(){
		return score;
	}
}