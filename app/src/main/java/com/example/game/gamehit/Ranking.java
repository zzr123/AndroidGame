package com.example.game.gamehit;

/**
 * Created by Administrator on 2017/12/8.
 */

import io.realm.RealmObject;

public class Ranking extends RealmObject{
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
