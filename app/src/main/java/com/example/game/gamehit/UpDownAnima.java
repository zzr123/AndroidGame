package com.example.game.gamehit;

/**
 * Created by Administrator on 2017/12/7.
 */

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class UpDownAnima {
	private Bitmap animaImg;//图片（背景图）
	private int animaImgHeight;// 图片的高度
	private int animaImgWidth;// 图片的宽度
	private long animaTime;//时间
	private int animaFrame;//帧数
	private boolean upDownFlag;//上下标识:true-up;false-down
	private Bitmap drawImg;// 获取图片信息
	private Bitmap imgDidong;// 获取图片信息（老鼠出来时的地洞）
	private int curFrame;//当前帧数
	private int drawX;// 确定矩形的顶点的横坐标
	private int drawY;// 确定矩形的顶点的纵坐标
	private long preDrawTime;//  开始的时间
	private boolean isHit;// 判断是否打击到
	private Rect hitRect;// 确定一块矩形区域
	private boolean hitable;//判断是否可以打击
	private int row;// 行
	private int col;//列
	private boolean addFlag;//设置一个flag，判断打倒的地鼠是加分(true)还是扣分(false)
	private int score;// 分数

	public UpDownAnima(Bitmap img, Bitmap imgDidong, long time, int frame, int drawx, int drawy, boolean addFlag) {
		this.animaImg = img;
		this.animaTime = time;
		this.animaFrame = frame;
		this.upDownFlag = true;
		this.isHit = false;
		this.animaImgHeight = img.getHeight();
		this.animaImgWidth = img.getWidth();

		this.curFrame = 1;
		this.drawX = drawx;
		this.drawY = drawy;
        //左上角的点的坐标 为（this.drawX-20, this.drawY-20）
		// 右下角的点的坐标为（this.drawX+this.animaImgWidth+20, this.drawY+this.animaImgHeight+20）
		// 老鼠出现的矩形区域
		this.hitRect = new Rect(this.drawX-20, this.drawY-20, this.drawX+this.animaImgWidth+20, this.drawY+this.animaImgHeight+20);

		this.hitable = true;
		this.imgDidong = imgDidong;
		this.addFlag = addFlag;
		this.score = this.addFlag?10:-10;
	}

    //绘制地鼠up和down时的动画
	public void drawFrame(Canvas canvas, Paint paint) {
		if(hitable){
			if(preDrawTime==0 ||(System.currentTimeMillis()-preDrawTime)>=animaTime){
				if(upDownFlag){
					curFrame++;
					upDownFlag = curFrame==animaFrame?false:true;
				}else{
					curFrame--;
					upDownFlag = curFrame==-1?true:false;
				}
				preDrawTime = System.currentTimeMillis();// 获取系统当前的时间
			}
		}
		int hightNum = curFrame>=animaFrame?animaImgHeight-10:
				curFrame<=0?2:(animaImgHeight/animaFrame*curFrame);
		if(hightNum==2){
			hitable = false;
		}
		// 在指定的xy值的地方显现地洞
		canvas.drawBitmap(imgDidong, drawX, drawY, null);
		drawImg = Bitmap.createBitmap(animaImg, 0, 0, animaImgWidth, hightNum);
		canvas.drawBitmap(drawImg, drawX+(imgDidong.getWidth()-drawImg.getWidth())/2,
					drawY+imgDidong.getHeight()/2-hightNum, paint);
	}

	//判断是否打击到地鼠
	public boolean isHit(int touchX, int touchY){
		//点击在老鼠的坐标内
		if(hitRect.contains(touchX, touchY)){
			isHit = true;
		}else{
			isHit = false;
		}
		return isHit;
	}

	//获取判断是否打到地鼠的结果
	public boolean isHit() {
		return isHit;
	}

	//判断地鼠是否可打击
	public boolean isHitable() {
		return hitable;
	}

	//获取行数
	public int getRow() {
		return row;
	}

	//设置行数
	public void setRow(int row) {
		this.row = row;
	}

	//获取列数
	public int getCol() {
		return col;
	}

	//设置列数
	public void setCol(int col) {
		this.col = col;
	}

	//判断是否加分
	public boolean isAddFlag() {
		return addFlag;
	}

	//获取分数
	public int getScore() {
		return score;
	}
}
