package com.example.game.gamehit;

/**
 * Created by Administrator on 2017/12/6.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

import static java.security.AccessController.getContext;

public class MenuActivity extends Activity {

	private Context mContext;
	private MediaPlayer mBgMediaPlayer;
	private String ranklist[];
	private Realm realm;
	private RealmResults<Ranking> userList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);//无标题
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);//全屏
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);   //应用运行时，保持屏幕高亮，不锁屏
		//初始化
		init();
		SharedPreferences sharedPreferences = getSharedPreferences("music", Activity.MODE_PRIVATE);
		boolean isplay = sharedPreferences.getBoolean("play",true);
		if (isplay) {
			playBGM();
		}
		setContentView(R.layout.activity_main);
		//设置各个按钮的监听器
		Button btnLevel = (Button) findViewById(R.id.btnRanking);
		btnLevel.setOnClickListener(lisn);
		Button btnRandom = (Button) findViewById(R.id.btnSetting);
		btnRandom.setOnClickListener(lisn);
		Button btnTimer = (Button) findViewById(R.id.btnStartGame);
		btnTimer.setOnClickListener(lisn);
		Button btnSuper = (Button) findViewById(R.id.btnExit);
		btnSuper.setOnClickListener(lisn);
		Button btnInfo = (Button) findViewById(R.id.btnGameIntroduction);
		btnInfo.setOnClickListener(lisn);
	}

	View.OnClickListener lisn = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(mContext, GameActivity.class);
			switch (v.getId()) {
				case R.id.btnGameIntroduction:
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setInverseBackgroundForced(true);
					TextView customTitleView = new TextView(mContext);
					customTitleView.setText("游戏说明");
					customTitleView.setTextSize(24);
					customTitleView.setGravity(Gravity.CENTER);
					customTitleView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
					customTitleView.setTextColor(getResources().getColor(R.color.fontColor));
					builder.setCustomTitle(customTitleView);
					builder.setMessage("1,地鼠出洞的瞬间将其击晕，根据被击晕的种类获得相应的奖励或惩罚\n " +
							"2,每关90秒游戏时间结束后，本关得分大于过关分数即可过关");
					builder.setPositiveButton("返回主菜单", null);
					builder.show();
					return;
				case R.id.btnRanking:
					final AlertDialog.Builder builder_sort = new AlertDialog.Builder(MenuActivity.this);
					builder_sort.setTitle("排行榜:");
					try {
						userList = realm.where(Ranking.class).findAll();
						Integer number[]=new Integer[userList.size()];
						for (int i=0;i<userList.size();i++){
							number[i]=userList.get(i).getNumber();
						}
						if (number.length != 0){
							Arrays.sort(number,Collections.reverseOrder());
							ranklist=new String[userList.size()];
							for (int i=0;i<userList.size();i++){
								ranklist[i]=String.valueOf(number[i]);
							}
						}
					}catch (Exception e){
						e.printStackTrace();
					}
					if (ranklist == null) {
						builder_sort.setItems(new String[]{"暂无数据"}, null);
					} else {
						builder_sort.setItems(ranklist, null);
					}

					builder_sort.setPositiveButton("清空", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialogInterface, int i){
							realm.beginTransaction();//必须先开启事务
							userList.deleteAllFromRealm();//删除所有数据
							realm.commitTransaction();//提交事务
							ranklist = null;
						}
					});

					builder_sort.setNegativeButton("确定", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
						}
					});
					builder_sort.show();
					break;
				case R.id.btnSetting:
					SharedPreferences sharedPreferences = getSharedPreferences("music", Activity.MODE_PRIVATE);
					boolean isplay = sharedPreferences.getBoolean("play", true);
					final SharedPreferences.Editor editor = sharedPreferences.edit();
					AlertDialog.Builder builder_set = new AlertDialog.Builder(MenuActivity.this);
					builder_set.setTitle("提示");
					builder_set.setMultiChoiceItems(new String []{"背景音乐"}, new boolean[]{isplay}, new DialogInterface.OnMultiChoiceClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i, boolean isChecked) {
							if (isChecked) {
								editor.putBoolean("play", true);
								playBGM();
								mBgMediaPlayer.start();
								Const.backgroundMusicOn = true;
							} else {
								editor.putBoolean("play", false);
								mBgMediaPlayer.pause();
								Const.backgroundMusicOn = false;
							}
							editor.commit();
						}
					});
					builder_set.setPositiveButton("返回", null);
					builder_set.show();
					break;
				case R.id.btnStartGame:
					startActivity(intent);
					break;
				case R.id.btnExit:
					finish();
					break;
			}
		}

	};


	private void init() {
		mContext = MenuActivity.this;
		Realm.init(this);
		RealmConfiguration config = new RealmConfiguration.Builder()
				.name("realmnum.realm")
				.schemaVersion(0)
				.build();
		realm = Realm.getInstance(config);
		//分辨率
		DisplayMetrics mDisplayMetrics = new DisplayMetrics();
		//将当前窗口的一些信息放在DisplayMetrics类中
		getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
		//获取当前手机屏幕宽和高的像素单位
		Const.CURRENT_SCREEN_WIDTH = mDisplayMetrics.widthPixels;
		Const.CURRENT_SCREEN_HEIGHT = mDisplayMetrics.heightPixels;
		//获取当前缩放比例
		Const.CURREN_SCALE = Const.CURRENT_SCREEN_WIDTH/Const.DEF_SCREEN_WIDTH;
		Const.CURREN_WIDTH_SCALE = Const.CURRENT_SCREEN_WIDTH/Const.DEF_SCREEN_WIDTH;
		Const.CURREN_HEIGHT_SCALE = Const.CURRENT_SCREEN_HEIGHT/Const.DEF_SCREEN_HEIGHT;
		//计算当前块宽高度
		Const.CURRENT_BLOCK_WIDTH_HEIGHT = Const.CURREN_SCALE*Const.DEF_BLOCK_WIDTH_HEIGHT;
		Const.CURRENT_BLOCK_WIDTH = Const.CURREN_WIDTH_SCALE*Const.DEF_BLOCK_WIDTH_HEIGHT;
		Const.CURRENT_BLOCK_HEIGHT = Const.CURREN_HEIGHT_SCALE*Const.DEF_BLOCK_WIDTH_HEIGHT;
	}

	//播放背景音乐
	private void playBGM(){
		mBgMediaPlayer = MediaPlayer.create(mContext, R.raw.background);
		mBgMediaPlayer.setLooping(true);//循环
	}

	//按返回键时，调用existGame方法，提示用户是否确认退出
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
				existGame();
				return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	//提示用户是否确认退出
	private void existGame(){
		TextView textView = new TextView(mContext);
		textView.setText("要退出游戏吗？");
		textView.setTextSize(24);
		textView.setGravity(Gravity.CENTER);
		textView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
		textView.setTextColor(getResources().getColor(R.color.fontColor));
		new AlertDialog.Builder(mContext)
				.setCustomTitle(textView)
				.setPositiveButton("否", null)
				.setNegativeButton("是", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				})
				.show();
	}

	@Override
	protected void onResume() {
		if(Const.backgroundMusicOn && mBgMediaPlayer!=null && !mBgMediaPlayer.isPlaying()){
			mBgMediaPlayer.start();
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		if (this.mBgMediaPlayer != null && this.mBgMediaPlayer.isPlaying()) {
			this.mBgMediaPlayer.pause();
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if (this.mBgMediaPlayer != null) {
			this.mBgMediaPlayer.stop();
			this.mBgMediaPlayer.release();
			this.mBgMediaPlayer = null;
		}
		super.onDestroy();
	}
}
