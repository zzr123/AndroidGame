package com.example.game.gamehit;

/**
 * Created by Administrator on 2017/12/6.
 */

public class Const {
	//默认屏幕宽度
	public static final float DEF_SCREEN_WIDTH = 480;
	//当前屏幕宽度
	public static float CURRENT_SCREEN_WIDTH = 480;
	//默认屏幕高度
	public static final float DEF_SCREEN_HEIGHT = 800;
	//当前屏幕高度
	public static float CURRENT_SCREEN_HEIGHT = 800;
	//默认块宽高度
	public static final float DEF_BLOCK_WIDTH_HEIGHT = 48;
	//当前块宽高度
	public static float CURRENT_BLOCK_WIDTH_HEIGHT = 48;
	public static float CURRENT_BLOCK_WIDTH = 48;
	public static float CURRENT_BLOCK_HEIGHT = 48;
	//默认缩放比例
	public static final float DEF_SCALE = 1;
	//当前绽放比例
	public static float CURREN_SCALE = 1;
	public static float CURREN_WIDTH_SCALE = 1;
	public static float CURREN_HEIGHT_SCALE = 1;

	//资源的位置固定的常数，背景图
	public static final int backgroundImgResid=R.drawable.mapds48_001;
	//数字地图放入资源位置
	public static final int gameArrayStrResid=R.string.didong_level;
	//背景音乐的资源位置
	public static final int voiceBackground= R.raw.time;
	//每局游戏的时间
	public static int timeNum=90;
	//默认背景音乐是开启的
	public static boolean backgroundMusicOn = true;
}
