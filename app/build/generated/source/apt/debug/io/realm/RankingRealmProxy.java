package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.SharedRealm;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RankingRealmProxy extends com.example.game.gamehit.util.Ranking
    implements RealmObjectProxy, RankingRealmProxyInterface {

    static final class RankingColumnInfo extends ColumnInfo
        implements Cloneable {

        public long numberIndex;

        RankingColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(1);
            this.numberIndex = getValidColumnIndex(path, table, "Ranking", "number");
            indicesMap.put("number", this.numberIndex);

            setIndicesMap(indicesMap);
        }

        @Override
        public final void copyColumnInfoFrom(ColumnInfo other) {
            final RankingColumnInfo otherInfo = (RankingColumnInfo) other;
            this.numberIndex = otherInfo.numberIndex;

            setIndicesMap(otherInfo.getIndicesMap());
        }

        @Override
        public final RankingColumnInfo clone() {
            return (RankingColumnInfo) super.clone();
        }

    }
    private RankingColumnInfo columnInfo;
    private ProxyState proxyState;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("number");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    RankingRealmProxy() {
        if (proxyState == null) {
            injectObjectContext();
        }
        proxyState.setConstructionFinished();
    }

    private void injectObjectContext() {
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (RankingColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState(com.example.game.gamehit.util.Ranking.class, this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @SuppressWarnings("cast")
    public int realmGet$number() {
        if (proxyState == null) {
            // Called from model's constructor. Inject context.
            injectObjectContext();
        }

        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.numberIndex);
    }

    public void realmSet$number(int value) {
        if (proxyState == null) {
            // Called from model's constructor. Inject context.
            injectObjectContext();
        }

        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.numberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.numberIndex, value);
    }

    public static RealmObjectSchema createRealmObjectSchema(RealmSchema realmSchema) {
        if (!realmSchema.contains("Ranking")) {
            RealmObjectSchema realmObjectSchema = realmSchema.create("Ranking");
            realmObjectSchema.add(new Property("number", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED));
            return realmObjectSchema;
        }
        return realmSchema.get("Ranking");
    }

    public static Table initTable(SharedRealm sharedRealm) {
        if (!sharedRealm.hasTable("class_Ranking")) {
            Table table = sharedRealm.getTable("class_Ranking");
            table.addColumn(RealmFieldType.INTEGER, "number", Table.NOT_NULLABLE);
            table.setPrimaryKey("");
            return table;
        }
        return sharedRealm.getTable("class_Ranking");
    }

    public static RankingColumnInfo validateTable(SharedRealm sharedRealm, boolean allowExtraColumns) {
        if (sharedRealm.hasTable("class_Ranking")) {
            Table table = sharedRealm.getTable("class_Ranking");
            final long columnCount = table.getColumnCount();
            if (columnCount != 1) {
                if (columnCount < 1) {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is less than expected - expected 1 but was " + columnCount);
                }
                if (allowExtraColumns) {
                    RealmLog.debug("Field count is more than expected - expected 1 but was %1$d", columnCount);
                } else {
                    throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field count is more than expected - expected 1 but was " + columnCount);
                }
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < columnCount; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final RankingColumnInfo columnInfo = new RankingColumnInfo(sharedRealm.getPath(), table);

            if (!columnTypes.containsKey("number")) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Missing field 'number' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("number") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Invalid type 'int' for field 'number' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.numberIndex)) {
                throw new RealmMigrationNeededException(sharedRealm.getPath(), "Field 'number' does support null values in the existing Realm file. Use corresponding boxed type for field 'number' or migrate using RealmObjectSchema.setNullable().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), "The 'Ranking' class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_Ranking";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.example.game.gamehit.util.Ranking createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.example.game.gamehit.util.Ranking obj = realm.createObjectInternal(com.example.game.gamehit.util.Ranking.class, true, excludeFields);
        if (json.has("number")) {
            if (json.isNull("number")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'number' to null.");
            } else {
                ((RankingRealmProxyInterface) obj).realmSet$number((int) json.getInt("number"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.example.game.gamehit.util.Ranking createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        com.example.game.gamehit.util.Ranking obj = new com.example.game.gamehit.util.Ranking();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("number")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'number' to null.");
                } else {
                    ((RankingRealmProxyInterface) obj).realmSet$number((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        obj = realm.copyToRealm(obj);
        return obj;
    }

    public static com.example.game.gamehit.util.Ranking copyOrUpdate(Realm realm, com.example.game.gamehit.util.Ranking object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().threadId != realm.threadId) {
            throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
        }
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return object;
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.example.game.gamehit.util.Ranking) cachedRealmObject;
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static com.example.game.gamehit.util.Ranking copy(Realm realm, com.example.game.gamehit.util.Ranking newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.example.game.gamehit.util.Ranking) cachedRealmObject;
        } else {
            // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
            com.example.game.gamehit.util.Ranking realmObject = realm.createObjectInternal(com.example.game.gamehit.util.Ranking.class, false, Collections.<String>emptyList());
            cache.put(newObject, (RealmObjectProxy) realmObject);
            ((RankingRealmProxyInterface) realmObject).realmSet$number(((RankingRealmProxyInterface) newObject).realmGet$number());
            return realmObject;
        }
    }

    public static long insert(Realm realm, com.example.game.gamehit.util.Ranking object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.example.game.gamehit.util.Ranking.class);
        long tableNativePtr = table.getNativeTablePointer();
        RankingColumnInfo columnInfo = (RankingColumnInfo) realm.schema.getColumnInfo(com.example.game.gamehit.util.Ranking.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.numberIndex, rowIndex, ((RankingRealmProxyInterface)object).realmGet$number(), false);
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.game.gamehit.util.Ranking.class);
        long tableNativePtr = table.getNativeTablePointer();
        RankingColumnInfo columnInfo = (RankingColumnInfo) realm.schema.getColumnInfo(com.example.game.gamehit.util.Ranking.class);
        com.example.game.gamehit.util.Ranking object = null;
        while (objects.hasNext()) {
            object = (com.example.game.gamehit.util.Ranking) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo.numberIndex, rowIndex, ((RankingRealmProxyInterface)object).realmGet$number(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.example.game.gamehit.util.Ranking object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.example.game.gamehit.util.Ranking.class);
        long tableNativePtr = table.getNativeTablePointer();
        RankingColumnInfo columnInfo = (RankingColumnInfo) realm.schema.getColumnInfo(com.example.game.gamehit.util.Ranking.class);
        long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
        cache.put(object, rowIndex);
        Table.nativeSetLong(tableNativePtr, columnInfo.numberIndex, rowIndex, ((RankingRealmProxyInterface)object).realmGet$number(), false);
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.game.gamehit.util.Ranking.class);
        long tableNativePtr = table.getNativeTablePointer();
        RankingColumnInfo columnInfo = (RankingColumnInfo) realm.schema.getColumnInfo(com.example.game.gamehit.util.Ranking.class);
        com.example.game.gamehit.util.Ranking object = null;
        while (objects.hasNext()) {
            object = (com.example.game.gamehit.util.Ranking) objects.next();
            if(!cache.containsKey(object)) {
                if (object instanceof RealmObjectProxy && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy)object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                    cache.put(object, ((RealmObjectProxy)object).realmGet$proxyState().getRow$realm().getIndex());
                    continue;
                }
                long rowIndex = Table.nativeAddEmptyRow(tableNativePtr, 1);
                cache.put(object, rowIndex);
                Table.nativeSetLong(tableNativePtr, columnInfo.numberIndex, rowIndex, ((RankingRealmProxyInterface)object).realmGet$number(), false);
            }
        }
    }

    public static com.example.game.gamehit.util.Ranking createDetachedCopy(com.example.game.gamehit.util.Ranking realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.example.game.gamehit.util.Ranking unmanagedObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.example.game.gamehit.util.Ranking)cachedObject.object;
            } else {
                unmanagedObject = (com.example.game.gamehit.util.Ranking)cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            unmanagedObject = new com.example.game.gamehit.util.Ranking();
            cache.put(realmObject, new RealmObjectProxy.CacheData(currentDepth, unmanagedObject));
        }
        ((RankingRealmProxyInterface) unmanagedObject).realmSet$number(((RankingRealmProxyInterface) realmObject).realmGet$number());
        return unmanagedObject;
    }

    @Override
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Ranking = [");
        stringBuilder.append("{number:");
        stringBuilder.append(realmGet$number());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RankingRealmProxy aRanking = (RankingRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aRanking.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aRanking.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aRanking.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }

}
