package io.realm;


public interface RankingRealmProxyInterface {
    public int realmGet$number();
    public void realmSet$number(int value);
}
